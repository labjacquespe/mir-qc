# miR-QC

microRNA-seq quantification quality control 

# Prepare environments

```bash

# conda env
wget https://repo.anaconda.com/miniconda/Miniconda3-py37_4.10.3-Linux-x86_64.sh
bash Miniconda3-py37_4.10.3-Linux-x86_64.sh -p /path/for/environments/miniconda3/
exec bash
conda config --set auto_activate_base False
conda install -n base -c conda-forge mamba
mamba create -c bioconda -c conda-forge -n <env_name> snakemake r-peer
conda activate <env_name>

```

# Prepare config file 

Add file paths to [config file](config.json) and determine pipeline parameters.


# Launch Pipeline 
```bash

# visualize pipeline steps 
snakemake --dag | dot -Tpdf > dag.snakemake.pdf

# launch snakemake pipeline 
snakemake --use-envmodules --cores=1

snakemake --use-envmodules --jobs 99 --cluster-config cluster.json --cluster "python3 ~/slurmSubmit.py"

# create snakemake report HTML
cp config.json /path/to/snakemake/outputs/.
snakemake --report report.html

```

