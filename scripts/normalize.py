#!/usr/bin/env python3
# Author: Francois Aguet
# adapted by Frederique White
import sys
import numpy as np
import pandas as pd
import qtl.norm
import os

counts_file = snakemake.input.counts
count_threshold = int(snakemake.params.count_threshold)
sample_frac_threshold = float(snakemake.params.sample_frac_threshold)
tmm_file = snakemake.output.tmm
#log_tmm_file = snakemake.output.log_tmm
int_tmm_file = snakemake.output.int_tmm

counts_df = pd.read_csv(counts_file, sep='\t', index_col=0)
ix = np.unique(counts_df.columns)
counts_df = counts_df[ix]
ns = counts_df.shape[1]
mask = (np.sum(counts_df >= count_threshold, axis=1) >= sample_frac_threshold * ns ).values

tmm_counts_df = qtl.norm.edger_cpm(counts_df, normalized_lib_sizes=True)
tmm_counts_df = tmm_counts_df[mask]
norm_df = qtl.norm.inverse_normal_transform(tmm_counts_df)
#log_df = np.log2(tmm_counts_df)

tmm_counts_df.to_csv(tmm_file, sep='\t')
#log_df.to_csv(log_tmm_file, sep='\t')
norm_df.to_csv(int_tmm_file, sep='\t')
