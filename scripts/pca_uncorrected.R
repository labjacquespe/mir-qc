# Author: Frederique White

# libraries
suppressMessages(library(corrplot))
suppressMessages(library(magrittr))
suppressMessages(library(matrixStats))
suppressMessages(library(ggplot2))
suppressMessages(library(ggforce))

# snakemake input
tmm_transfo_file = snakemake@input[[1]]

# snakemake params
covariates_file = snakemake@params[[1]]

# snakemake outputs
pcs_file = snakemake@output[[1]]
plot_file = snakemake@output[[2]]

log <- file(as.character(snakemake@log), open="a")


# analysis

df <- read.csv(tmm_transfo_file, sep="\t", header=T, check.names=F, row.names=1)

cov <- read.csv(covariates_file, sep="\t", header=T, check.names=F)
cov$Gender <- ifelse(cov$Gender=="MALE", 0, 1)

# run PCA
pc <- prcomp(t(df))

# get variance:
var <- summary(pc)$importance[2,]
Var <- data.frame("x"=1:length(var), "var"=as.vector(var)*100)

# get PCs
pcs <- as.data.frame(pc$x)
pcs <- cbind("ID"=rownames(pcs), pcs)
write.table(pcs, file=pcs_file, sep="\t",row.names=F, quote=F)

pcs <- merge(cov, pcs, by.x="Gen3GID", by.y="ID")
pcs$order <- rownames(pcs)
subcov <- pcs[, c(5,10,11,18,20,24,29,39:48)]
M_covariates <- abs(cor(subcov))


# plots 

pdf(plot_file)
corrplot(M_covariates, type = 'lower', col.lim=c(0, 1), tl.col = 'black') %>% corrRect(c(1,8,17)) # , col=COL1('YlGn', 10)
ggplot(Var, aes(x=x, y=var)) + geom_point(size=3, alpha=0.5) + ggtitle("Scree plot") + xlab("PCs")+ ylab("Variance (%)")
ggplot(Var[1:20,], aes(x=x, y=var)) + geom_point(size=3, alpha=0.5) + ggtitle("Scree plot") + xlab("PCs")+ ylab("Variance (%)")
pcs$Batch <- as.factor(pcs$Batch)
ggplot(pcs, aes(x= .panel_x, y= .panel_y, color=Batch)) + geom_point(alpha=1, shape=16, size=0.5) + facet_matrix(vars(PC1, PC2, PC3, PC4, PC5))
ggplot(pcs, aes(x=PC1, y=PC2, color=Batch)) + geom_point(size=2, alpha=0.5) + xlab(paste0("PC1 (",Var$var[1] ," %)"))+ ylab(paste0("PC2 (",Var$var[2] ," %)")) + geom_text(data=subset(pcs, (abs(PC1) > 3*sd(PC1)) | (abs(PC2) > 3*sd(PC2))), aes(label=Gen3GID), nudge_y=-1)
ggplot(pcs, aes(x=PC1, y=PC2, color=RIN)) + geom_point(size=2, alpha=0.5) + xlab(paste0("PC1 (",Var$var[1] ," %)"))+ ylab(paste0("PC2 (",Var$var[2] ," %)"))+scale_color_gradient(low="blue", high="red")
ggplot(pcs, aes(x=PC1, y=PC2, color=Filtered_miRNA_Reads)) + geom_point(size=2, alpha=0.5) + xlab(paste0("PC1 (",Var$var[1] ," %)"))+ ylab(paste0("PC2 (",Var$var[2] ," %)"))+scale_color_gradient(low="blue", high="red")
dev.off()

