# Author: Frederique White

library(matrixStats)
library(ggplot2)
library(reshape2)

# snakemake input
counts_file = snakemake@input[[1]]

# snakemake params
sample_file = snakemake@params[["samples"]]
#count_threshold = snakemake@params[["count_threshold"]]
#sample_frac_threshold = snakemake@params[["sample_frac_threshold"]]

# snakemake outputs
sensitivity_analysis = snakemake@output[["sa"]]
gene_stats_file = snakemake@output[["stats"]]
clean_count_file = snakemake@output[["counts"]]
pseudo_count_file = snakemake@output[["pseudo"]]
plot_file = snakemake@output[["plot"]]

log <- file(as.character(snakemake@log), open="a")

cat('Clean expression file', file=log, sep="\n")
counts <- read.csv(counts_file, sep="\t", header=T, check.names=F)
rownames(counts) <- counts[,1]
counts <- counts[, -1]
cat(paste0("Initial number of samples : ", dim(counts)[2]), file=log, sep="\n")
cat(paste0("Initial number of genes : ", dim(counts)[1]), file=log, sep="\n")

# filter samples
samples <- read.csv(sample_file, sep="\t", header=T) # SampleID Gen3GID
counts <- counts[, samples$SampleID]
cat(paste0("Number of samples included : ", dim(counts)[2]), file=log, sep="\n")

# map Gen3G ID
rownames(samples) <- samples$SampleID
names(counts) <- samples[names(counts),]$Gen3GID

# remove all zero rows
counts <- counts[rowSums(counts)>0, ]
cat(paste0("All-zero genes removed : ", dim(counts)[1]), file=log, sep="\n")

# sensitivity analysis for threshold
tmp <- counts
tmp$reads <- rowSums(counts)
tmp$zeros <- rowSums(counts == 0)
tmp$min1 <- rowSums(counts >= 1)
tmp$min2 <- rowSums(counts >= 2)
tmp$min3 <- rowSums(counts >= 3)
tmp$min4 <- rowSums(counts >= 4)
tmp$min5 <- rowSums(counts >= 5)
tmp$min6 <- rowSums(counts >= 6)
tmp$min7 <- rowSums(counts >= 7)
tmp$min8 <- rowSums(counts >= 8)
tmp$min9 <- rowSums(counts >= 9)
tmp$min10 <- rowSums(counts >= 10)
tmp <- tmp[, 471:ncol(tmp)]
tmp <- cbind("gene_name"=rownames(tmp), tmp)
write.table(tmp, file=sensitivity_analysis, sep="\t", quote=F,row.names=F)

# filter genes
#cat(paste0("Count threshold : ", count_threshold), file=log, sep="\n")
#cat(paste0("Sample fraction threshold : ", sample_frac_threshold), file=log, sep="\n")
#counts_in <- apply(counts, 1, function(x) sum(x >= count_threshold)/length(x) >= sample_frac_threshold)
#counts_in <- as.data.frame(counts[counts_in,])
#cat(paste0("Excluded genes : ", (dim(counts)[1]- dim(counts_in)[1])), file=log, sep="\n")
#cat(paste0("Included genes : ", dim(counts_in)[1]), file=log, sep="\n")
counts_in <- as.data.frame(counts[,])

# look for gene outliers
# get rid of sample with to much reads 
tmp <- counts_in
tmp[,"1118"] <- NULL
genes <- data.frame(
	"gene"=rownames(tmp), 
	"all"=rowSums(tmp),
	"min"=rowMins(as.matrix(tmp)), 
	"max"=rowMaxs(as.matrix(tmp)), 
	"mean"=rowMeans(as.matrix(tmp)), 
	"median"=rowMedians(as.matrix(tmp)), 
	"var"=rowVars(as.matrix(tmp)),
	"SD"=rowSds(as.matrix(tmp)),
	"zeros"=rowSums(tmp==0)
	)
write.table(genes, file=gene_stats_file, sep="\t", quote=F,row.names=F)
genes$RSD <- genes$SD/genes$mean
genes <- genes[order(genes$all, decreasing=T),]
genes$order <- 1:nrow(genes)
genes$prop_total <- (genes$all/sum(genes$all))
genes$cum_prop_total <- cumsum(genes$prop_total)

l <- c(0.25,0.5,0.75,0.9,0.99)
L <- list()
for (i in 1:length(l)) {L[i] <- dim(genes[genes$cum_prop_total <= l[i], ])[1]}
data <- data.frame("cum_prop_total"=l, "order"=unlist(L))
high_prop <- subset(genes, cum_prop_total <= 0.25)

m <- as.data.frame(t(tmp))
m$all <- rowSums(m)
m$samples <- rownames(m)
m <- m[order(m$all),]
m$order <- 1:nrow(m)
m_melted <- melt(m, id=c("samples","order","all"))
m_melted$prop <- (m_melted$value/m_melted$all)*100
m_melted_high_prop <- m_melted[m_melted$variable %in% high_prop$gene, ]
m_melted_high_prop_one <- m_melted[m_melted$variable == high_prop$gene[1], ]

# figures 
pdf(plot_file)
ggplot(genes, aes(x=log2(all), y=zeros)) + geom_point(aes(alpha=0.5), show.legend=F)+ geom_text(data=subset(genes, (zeros > 50) & (log2(all) > 15 )), aes(label=gene), check_overlap=T, nudge_y=-8) + xlab("Total counts") + ylab("Samples with zero count")
ggplot(genes, aes(x=order, y=cum_prop_total*100)) + geom_line() + ylab("Cumulative Proportion of total reads") +xlab("Genes") + geom_segment(data=data, aes(x=0, y=cum_prop_total*100, xend=order, yend=cum_prop_total*100)) + geom_text(data=data, aes(label=order), nudge_x=40, nudge_y=-2)
ggplot(m_melted_high_prop, aes(x=order)) + geom_line(aes(y=value, color=variable, alpha=0.5)) + geom_line(aes(y=all/10)) + scale_y_continuous(name="Counts",sec.axis=sec_axis(~.*10, name="Total counts"))+ xlab("Samples") # + theme(legend.position="bottom")
ggplot(m_melted_high_prop, aes(x=order)) + geom_line(aes(y=log2(value), color=variable, alpha=0.5)) + geom_line(aes(y=log2(all))) + xlab("Samples") + ylab("Counts")
ggplot(m_melted_high_prop_one, aes(x=all, y=value)) + geom_point() + xlab("Total counts") + ylab(paste0(high_prop$gene[1], " (counts)"))
dev.off()


pdf(paste0(plot_file,"huge.pdf"), height=100, width=20)
ggplot(genes) + geom_segment(aes(x=reorder(gene, log2(mean)), xend=gene, y=log2(min), yend=log2(max)), color="black") + geom_point( aes(x=gene, y=log2(median)), color="blue", size=1 ) + geom_point( aes(x=gene, y=log2(mean)), color="red", size=1 ) + coord_flip() + xlab("Genes") + ylab("log2 Counts")
ggplot(m_melted, aes(x=order)) + geom_line(aes(y=value, color=variable, alpha=0.5)) + geom_line(aes(y=all/100)) + scale_y_continuous(name="Counts",sec.axis=sec_axis(~.*100, name="Total counts"))+ theme(legend.position="bottom")
dev.off()


# replace zero values by half the minimum non zero value per gene
nozeros <- counts_in
for ( i in 1:dim(nozeros)[1]){
	nozeros[i,][nozeros[i,]==0] <- min(nozeros[i,][nozeros[i,]>0])/2
}

# write outputs
counts_in <- cbind("gene_name"=rownames(counts_in), counts_in)
cat(paste0("writing raw counts in :", clean_count_file), file=log, sep="\n")
write.table(counts_in, clean_count_file, sep="\t", row.name=F, quote=F)

nozeros <- cbind("gene_name"=rownames(nozeros), nozeros)
cat(paste0("writing raw counts with pseudo counts in :", pseudo_count_file), file=log, sep="\n")
write.table(nozeros, pseudo_count_file, sep="\t", row.name=F, quote=F)
