#================================================================
#      _________   __________ ___    ____ ____________  _________ 
#     /   ____  \ /  _______/   /   /   //________   / /   ____  \     
#    /   /   /__//  /__    /    \  /   /      ___/  / /   /   /__/ 
#   /   / _____ /  ____/  /      \/   /      /__   / /   / ______    
#  /   / /__   /  /   ___/   /\      /___       \  \/   / /__   /  
#  \   \___/  /  /___/  /   /  \    / \  \______/  /\   \___/  /  
#   \ _______/_________/___/   /___/   \__________/  \________/  
#
#================================================================
# 						M I R S E Q  
#			  Q U A L I T Y    C O N T R O L 
#================================================================

configfile: "config.json"
workdir: "outputs/shared_version"

Transformations = ["int", "log", "voom"]

rule all:
	input:
		expand("PCA/uncorrected_pcs_{transformations}.tsv", transformations=Transformations),
		expand("PEER/peer_residuals_{transformations}.tsv", transformations=Transformations),
		#expand("PCA/corrected_pcs_{transformations}.tsv", transformations=Transformations)

# remove all-zero gene
# apply thresholds
# add pseudo_counts
rule clean:
	input:
			counts = config["file"]["counts"]
	output:
			sa = "data/sensitivity_analysis.tsv",
			stats = "data/counts_stats.tsv",
			counts = "data/clean_counts.tsv", 
			pseudo = "data/clean_counts_pseudo.tsv", 
			plot = report("plots/genes.pdf")
	params:
			samples = config["file"]["samples"]
	log:
			"log/clean.o"
	envmodules:	
			"StdEnv/2020", "gcc/9.3.0", "r/4.1.2", "r-bundle-bioconductor/3.14"
	script:
			"scripts/clean.R"

# compute TMM 
# inverse normal transformation
rule normalize:
	input:		
			counts = rules.clean.output.counts
	output:	
			tmm = report("normalized/normalized.tsv"),
			int_tmm = "normalized/normalized_int.tsv"
	params:
			count_threshold = config["count_threshold"],
			sample_frac_threshold = config["sample_frac_threshold"]
	log:	
			"log/normalize.o"
	envmodules:
			"tqtl/1.0.6" # pyenv for package "qtl"
	script:
			"scripts/normalize.py" # code from Francois Aguet

rule voom:
	input:
			raw = "data/clean_counts.tsv",
			norm = rules.normalize.output.tmm
	output:
			voom = "normalized/normalized_voom.tsv",
			log = "normalized/normalized_log.tsv"
	envmodules:	
			"StdEnv/2020", "gcc/9.3.0", "r/4.1.2", "r-bundle-bioconductor/3.14"
	script:
			"scripts/voom.R"

rule pca:
	input:		
			"normalized/normalized_{transformations}.tsv"
	params:	
			config["file"]["metrics"]
	output:
			"PCA/uncorrected_pcs_{transformations}.tsv",
			report("plots/uncorrected_pca_{transformations}.pdf")
	log:
			"log/uncorrected_pcs_{transformations}.o"
	envmodules:	
			"StdEnv/2020", "gcc/9.3.0", "r/4.1.2", "r-bundle-bioconductor/3.14"
	script:
			"scripts/pca_uncorrected.R"

# estimate peer factors 
rule peer:
	input:		
			tmm = "normalized/normalized_{transformations}.tsv"
	params:	
			factors = config["peer_factors"]
	output:	
			factors = "PEER/peer_{transformations}.tsv",
			residuals = report("PEER/peer_residuals_{transformations}.tsv"),
			plot = report("plots/peer_{transformations}.pdf")
	log:
			"log/peer_{transformations}.o"
	script:
			"scripts/peer_factors.R"


rule pca_corrected:
	input:	
			"PEER/peer_residuals_{transformations}.tsv",
			"PEER/peer_{transformations}.tsv"
	params:	
			config["file"]["metrics"]
	output:
			"PCA/corrected_pcs_{transformations}.tsv",
			report("plots/corrected_pca_{transformations}.pdf")
	log:
			"log/corrected_pca_{transformations}.o"
	envmodules:	
			"StdEnv/2020", "gcc/9.3.0", "r/4.1.2", "r-bundle-bioconductor/3.14"
	script:
			"scripts/pca_corrected.R"
